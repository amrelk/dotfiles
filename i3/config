# vim:ft=i3config:foldmethod=marker

#: settings {{{

#: bad place for this
exec_always --no-startup-id $HOME/.config/polybar/launch.sh
exec_always --no-startup-id $HOME/.cargo/bin/i3-auto-layout

#: borders {{{
set $border_size_float 4
#: Borders are only set for floating windows
default_border pixel 0
default_floating_border pixel $border_size_float
new_window pixel 0
new_float normal $border_size_float
#: hide borders when there's only one window
hide_edge_borders smart
#: }}}

#: titlebar {{{
#: font for window titles
font pango:Roboto 10
#: center title
title_align center
#: enthiccen title
for_window [class=".*"] title_format "<b>%title</b>"
#: }}}

#: gaps {{{
set $gaps_inner 4
set $gaps_variation 4
#: set inner/outer gaps
gaps inner $gaps_inner
gaps outer 0
#: defines whether gaps are used when there's only one window (on meaning no)
smart_gaps on
#: }}}

#: workspaces {{{

#: https://i3wm.org/docs/userguide.html#workspace_auto_back_and_forth
workspace_auto_back_and_forth yes

#: workspace names
set $ws1 1
set $ws2 2
set $ws3 3
set $ws4 4
set $ws5 5
set $ws6 6
set $ws7 7
set $ws8 8
set $ws9 9

#: workspace assignments
assign [class="chromium"]	$ws3
assign [class="discord"]	$ws4

#: }}}

#: i3 colors {{{

#: solarized
set $base03 #002b36
set $violet #6c71c4
set $red #dc322f

#: class		border	bg	fg	indic	child_border
client.focused		$violet	$violet	$base03	$violet
client.focused_inactive	$base03	$base03	$violet	$base03
client.unfocused	$base03	$base03	$violet	$base03
client.urgent		$red	$red	$base03	$red
client.placeholder	$base03	$base03	$red	$base03
client.background	$base03

#: }}}

#: }}}

#: keybindings {{{

set $mod Mod4
set $alt Mod1

#: misc {{{

floating_modifier $mod

#: kill focused window
bindsym $mod+q			kill
bindsym $mod+BackSpace		kill

#: volume
bindsym XF86AudioRaiseVolume	exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10%
bindsym XF86AudioLowerVolume	exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10%
bindsym XF86AudioMute		exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioMicMute	exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle

#: brightness
bindsym XF86MonBrightnessUp	exec --no-startup-id xbacklight -inc 10
bindsym XF86MonBrightnessDown	exec --no-startup-id xbacklight -dec 10

#: clipboard qr code
bindsym $mod+Shift+q            exec --no-startup-id xclip -selection c -o | qrencode -s 8 -o /tmp/qr_code.png && feh /tmp/qr_code.png && rm /tmp/qr_code.png

#: screenshot to clipboard
bindsym $mod+s                  exec --no-startup-id maim -s | xclip -selection clipboard -t image/png

#: }}}

#: window nav/magagement {{{

#: focus
bindsym $mod+h			focus left
bindsym $mod+j			focus down
bindsym $mod+k			focus up
bindsym $mod+l			focus right

#: move focused window
bindsym $mod+Shift+h		move left
bindsym $mod+Shift+j		move down
bindsym $mod+Shift+k		move up
bindsym $mod+Shift+l		move right

#: tiling direction
#bindsym $mod+h			split h
bindsym $mod+v			split v

#: flash! thunder!
bindsym $mod+w			exec --no-startup-id flash_window

#: fullscreen for current window
bindsym $mod+f			fullscreen toggle

#: floaters
bindsym $mod+Shift+space	floating toggle
bindsym $mod+space		focus mode_toggle
bindsym $mod+Shift+s		sticky toggle

#: scratchpad stuff
bindsym $mod+Shift+b		move scratchpad
bindsym	$mod+b			scratchpad show

#: gaps control
bindsym $mod+Shift+g            gaps inner current plus $gaps_variation
bindsym $mod+Ctrl+g             gaps inner current minus $gaps_variation
bindsym $mod+g                  gaps inner current set $gaps_inner

#: }}}

#: apps {{{

#: terminal
bindsym $mod+Return		exec kitty -1
bindsym $mod+Shift+Return	exec kitty -1 #: make me float pls

#: rofis
bindsym $mod+d			exec --no-startup-id rofi-appsmenu
bindsym $mod+0			exec --no-startup-id rofi-power

#: }}}

#: workspaces {{{

#: change monitor
bindsym $mod+$alt+l		move workspace to output right
bindsym $mod+$alt+h		move workspace to output left

#: guess
bindsym $mod+1			workspace $ws1
bindsym $mod+2			workspace $ws2
bindsym $mod+3			workspace $ws3
bindsym $mod+4			workspace $ws4
bindsym $mod+5			workspace $ws5
bindsym $mod+6			workspace $ws6
bindsym $mod+7			workspace $ws7
bindsym $mod+8			workspace $ws8
bindsym $mod+9			workspace $ws9

#: guess again
bindsym $mod+Ctrl+1		move container to workspace $ws1
bindsym $mod+Ctrl+2		move container to workspace $ws2
bindsym $mod+Ctrl+3		move container to workspace $ws3
bindsym $mod+Ctrl+4		move container to workspace $ws4
bindsym $mod+Ctrl+5		move container to workspace $ws5
bindsym $mod+Ctrl+6		move container to workspace $ws6
bindsym $mod+Ctrl+7		move container to workspace $ws7
bindsym $mod+Ctrl+8		move container to workspace $ws8
bindsym $mod+Ctrl+9		move container to workspace $ws9

#: huh this is neat
bindsym $mod+Shift+1		move container to workspace $ws1; workspace $ws1
bindsym $mod+Shift+2		move container to workspace $ws2; workspace $ws2
bindsym $mod+Shift+3		move container to workspace $ws3; workspace $ws3
bindsym $mod+Shift+4		move container to workspace $ws4; workspace $ws4
bindsym $mod+Shift+5		move container to workspace $ws5; workspace $ws5
bindsym $mod+Shift+6		move container to workspace $ws6; workspace $ws6
bindsym $mod+Shift+7		move container to workspace $ws7; workspace $ws7
bindsym $mod+Shift+8		move container to workspace $ws8; workspace $ws8
bindsym $mod+Shift+9		move container to workspace $ws9; workspace $ws9

#: }}}

#: }}}

#: modes {{{

#: resize {{{

bindsym $mod+r \
    exec --no-startup-id notify-send --icon=/usr/share/icons/Paper/scalable/actions/image-crop-symbolic.svg \
        "Resizing mode" \
        "Use the arrow keys to resize the active window"; \
    mode "resize"

mode "resize" {
    bindsym h		resize shrink width 10 px or 10 ppt
    bindsym j		resize grow height 10 px or 10 ppt
    bindsym k		resize shrink height 10 px or 10 ppt
    bindsym l		resize grow width 10 px or 10 ppt

    bindsym Return	mode "default"
    bindsym Escape	mode "default"
    bindsym $mod+r	mode "default"
}

#: }}}

#: restart {{{

bindsym $mod+x \
    exec --no-startup-id notify-send --icon=/usr/share/icons/Paper/scalable/actions/view-refresh-symbolic.svg \
        "Restart mode" \
        "<span color='#728cbb'></span><span weight='heavy' color='#ffffff' background='#728cbb'>D</span><span color='#728cbb' background='#333333'></span><span color='#e2e2e2' background='#333333'> dunst </span><span color='#333333'></span>  <span color='#728cbb'></span><span weight='heavy' color='#ffffff' background='#728cbb'>I</span><span color='#728cbb' background='#333333'></span><span color='#e2e2e2' background='#333333'> i3 </span><span color='#333333'></span>  <span color='#728cbb'></span><span weight='heavy' color='#ffffff' background='#728cbb'>P</span><span color='#728cbb' background='#333333'></span><span color='#e2e2e2' background='#333333'> polybar </span><span color='#333333'></span>  <span color='#728cbb'></span><span weight='heavy' color='#ffffff' background='#728cbb'>C</span><span color='#728cbb' background='#333333'></span><span color='#e2e2e2' background='#333333'> compton </span><span color='#333333'></span> "; \
    mode "restart"
mode "restart" {
    # Restart dunst (notifications)
    bindsym d                   exec --no-startup-id pkill dunst && dunst -config ~/.config/dunst/dunstrc; mode "default"
    # Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
    bindsym i                   restart; mode "default"
    # Restart the polybar (the pretty bar up top)
    bindsym p                   exec --no-startup-id ~/.config/polybar/launch.sh; mode "default"
    # Restart compton (compositor)
    bindsym c                   exec --no-startup-id pkill compton && compton &; mode "default"
    # Exit restart mode: "Enter" or "Escape"
    bindsym Return              mode "default"
    bindsym Escape              mode "default"
    bindsym $mod+x		mode "default"
}

#: }}}

#: }}}

